var express = require('express');
var app = express();

app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/public'));

app.get('/', function(request, response) {
  response.send('Servidor Node.js OK!');
});

app.listen(app.get('port'), function() {
  console.log("App Node.js executando em http://localhost:" + app.get('port'));
});
