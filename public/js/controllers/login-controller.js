aplicacao.controller('LoginController',
		function($http, $scope, $routeParams, $location, $timeout, SessaoService, Config) {

			if (SessaoService.login) {
				$location.path('/dashboard');
			}
	
			var baseURL = Config.url + Config.uri;
			
			$scope.mensagem = { erro : ''};
			$scope.btnentrar = true;
			$scope.btncarregando = false;

			/**
			 * Metodo autenticar 
			 * ===============================================
			 * 
			 * E invocado ao submeter o formulario, acessando o servico externo
			 * e efetuando o login.
			 */
			$scope.autenticar = function() {
				var recurso = baseURL+'/conta/login';
				var header = {
						'Accept' : 'application/json',
						'Content-Type' : 'application/x-www-form-urlencoded;'
					};
				var dados = $.param({
						login : $scope.autenticacao.login,
						senha : $scope.autenticacao.senha
					});
				
				var promisseHTTP = $http({method:'POST', url:recurso, data:dados, headers:header });
				preload(true);
				promisseHTTP.success(function(data, status, headers, config) {
					//alert("Autenticado com sucesso! " + data.nome + ", " + data.conta.email);
					SessaoService.criar(data);
					$location.path('/dashboard');
					console.log(data);
					console.log(headers);
				}).error(function(data, status, headers, config) {
					
					if(status == 400)
						$scope.mensagem = { erro:data.mensagem };
					else
						$scope.mensagem = { erro:"Não foi possível se comunicar com o serviço." };
				
				}).finally(function(data){
					preload(false);
				});

			};
			
			
			var preload = function(condicao){
				if(condicao) {
					$scope.btnentrar = false;
					$scope.btncarregando = true;
				} else {
					$scope.btnentrar = true;
					$scope.btncarregando = false;
				}
			};
		});
