aplicacao.controller('DashboardController', function($http, $scope,
		$routeParams, $location, SessaoService, Config) {

	console.log(SessaoService.getCookieStore);

	if (!SessaoService.login) {
		$location.path('/');
	}

	if(SessaoService.getCookieStore || SessaoService.login) {
		$scope.nome = SessaoService.nome;
		$scope.login = SessaoService.login;
		$scope.email = SessaoService.email;
	}
	
	$scope.logout = function() {
		if (confirm("Tem certeza que deseja sair?")) {
			SessaoService.excluir();
			$location.path('/');
		}
	};

});