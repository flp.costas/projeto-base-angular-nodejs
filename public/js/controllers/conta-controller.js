aplicacao.controller('ContaController', function($http, $scope, $routeParams,
		$location, $timeout, SessaoService, Config) {

	var baseURL = Config.url + Config.uri;

	$scope.mensagem = {
		erro : '',
		sucesso : ''
	};
	$scope.btncadastrar = true;
	$scope.btncarregando = false;

	$scope.cadastrar = function() {
		if(verifica_senha($scope.conta.senha, $scope.conta.confirmarsenha))
			requisicao_servico();
			
	};

	var verifica_senha = function(senha, senhaconfirmada) {
		if (senha != senhaconfirmada) {
			$scope.mensagem = {
				erro : "Confirmação de senha inválida."
			};
			return false;
		}
		return true;
	};
	
	
	var requisicao_servico = function() {
		
		var recurso = baseURL+'/conta/criar';
		var header = {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			};
		var dados = {
				nome : $scope.conta.nome,
				sobrenome : $scope.conta.sobrenome,
				sexo : $scope.conta.sexo,
				login : $scope.conta.login,
				email : $scope.conta.email,
				senha : $scope.conta.senha
			};
	
		var promisseHTTP = $http({method:'POST', url:recurso, data:dados, headers:header });
		preload(true);
		promisseHTTP.success(function(data, status, headers, config) {
			$scope.mensagem = { sucesso:"Cadastro efetuado com sucesso!" };
			console.log(data);
			console.log(headers);
		}).error(function(data, status, headers, config) {
			if(status == 400)
				$scope.mensagem = { erro:data.mensagem };
			else
				$scope.mensagem = { erro:"Não foi possível se comunicar com o serviço." };
		
		}).finally(function(data){
			preload(false);
		});
	};
	
	var preload = function(condicao){
		if(condicao) {
			$scope.btncadastrar = false;
			$scope.btncarregando = true;
		} else {
			$scope.btncadastrar = true;
			$scope.btncarregando = false;
		}
	};

});