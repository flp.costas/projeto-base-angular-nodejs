aplicacao.controller('PlaygroundController', function($cookieStore, $http, $scope,
		$routeParams, Fabrica, Servico, CONSTANTE, Config) {

	$cookieStore.remove('sessao');
	console.log(Servico);
	console.log(Fabrica);
	console.log(CONSTANTE);
	
	// nao é provado que esse recursos para angular server
	// efetivamente.
	// $http.defaults.useXDomain = true;
	// delete $http.defaults.headers.common['X-Requested-With'];

	var baseURL = Config.url + Config.uri;

	/**
	 * Metodo GET =============================================== Invoca metodo
	 * get ao servico. Disparado pelo evento de botao.
	 * 
	 */
	$scope.get = function() {
		var recurso = baseURL + '/get';
		var header = {
			'Accept' : 'application/json'
		};
		$http({
			method : 'GET',
			url : recurso,
			headers : header
		}).success(function(data, status, headers, config) {
			alert("servico carregado via GET... " + data.nome);
		}).error(function(data, status, headers, config) {
			alert('erro: ' + status);
		});
	};

	/**
	 * Metodo POST =============================================== Invoca botao
	 * post ao servico Disparado pelo evento de botao.
	 * 
	 */
	$scope.post = function() {
		var recurso = baseURL + '/post';
		var header = {
			'Content-Type' : 'application/json',
			'Accept' : 'application/json'
		};
		var dados = {
			login : "Costa",
			nome : "Filipe"
		};
		$http({
			method : 'POST',
			url : recurso,
			data : dados,
			headers : header
		}).success(function(data, status, headers, config) {
			alert("servico carregado via POST... " + data.nome);
		}).error(function(data, status, headers, config) {
			alert('erro: ' + status);
		});
	};
	
	/**
	 * Metodo POST =============================================== 
	 * Invoca servico de login
	 * 
	 */
	$scope.login = function() {
		var recurso = baseURL + '/conta/login';
		var header = {
			'Accept' : 'application/json',
			'Content-Type': 'application/x-www-form-urlencoded'
		};
		var dados = $.param({
			login : "flpcostas",
			senha : "1234"
		});
		$http({
			method : 'POST',
			url : recurso,
			data : dados,
			headers : header
		}).success(function(data, status, headers, config) {
			console.log(data);
			alert("servico carregado via POST... " + data.nome);
		}).error(function(data, status, headers, config) {
			alert('erro: ' + status);
		});
	};

});