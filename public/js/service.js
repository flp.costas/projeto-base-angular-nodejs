aplicacao.service('SessaoService', function($cookieStore) {
	
	var sessao = $cookieStore.get('sessao');
	var objeto = this;
	
	if(sessao) {
		this.login = sessao.login;
		this.nome = sessao.nome + " " + sessao.sobrenome;
		this.email = sessao.email;
	}
	
	this.criar = function(dadosSessao){
		this.nome = dadosSessao.nome + " " + dadosSessao.sobrenome;
		this.login = dadosSessao.login;
		this.email = dadosSessao.email;
		$cookieStore.put('sessao', dadosSessao);
	};
	
	this.excluir = function(){
		this.login = "";
		this.nome = "";
		this.email = "";
		$cookieStore.remove('sessao');
	};
	
	this.getCookieStore = $cookieStore.get('sessao');
	
	
	return this;
});