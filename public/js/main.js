var aplicacao = angular.module('projeto-base-angular-nodejs', [ 'ngRoute', 'ngCookies' ]);

// Definicao e configuracao de rotas
aplicacao.config(function($routeProvider, $httpProvider) {

	$routeProvider.when('/playground', {
		templateUrl : 'views/playground.html',
		controller : 'PlaygroundController'
	});

	$routeProvider.when('/login', {
		templateUrl : 'views/login.html',
		controller : 'LoginController'
	});

	$routeProvider.when('/dashboard', {
		templateUrl : 'views/dashboard.html',
		controller : 'DashboardController'
	});
	
	$routeProvider.when('/criar-conta', {
		templateUrl : 'views/conta-cadastro.html',
		controller : 'ContaController'
	});

	$routeProvider.otherwise({
		redirectTo : '/login'
	});
});

// Constantes - fornece dados
aplicacao.factory('Config', function() {
	return {
		url : 'http://projeto-java-ddd.herokuapp.com',
		//url : 'http://localhost:8080/projeto-java-base-ddd',
		uri : '/api/v1/servico'
	};
});

// Constantes - fornece dados
aplicacao.constant('CONSTANTE', {
	dados : 'dados e informacoes'
});

// Fabricas - fornece objetos de estado
aplicacao.factory('Fabrica', function() {
	return "Isso é uma fábrica!";
});

// Servicos - fornece funcoes
aplicacao.service('Servico', function() {
	return "Isso é um serviço!";
});
